#### LCDFraps plugin for LCDHype

The plugin shows the fps rate of any DirectX / OpenGL application running by using [Fraps](https://www.fraps.com).

It communicates with the `fraps.dll` in the plugin directory (that is the dll from *Fraps 1.9C* packed with the plugin)

##### How to update the plugin for using the recent version of Fraps?

The following steps were tested with *Fraps 3.5.99* and will hopefully work for future versions too.

1. Copy the plugin binaries into the designated folder, normally this would be `%ProgramFiles(x86)%\LCDHype\plugins\lcdfraps`
2. Install Fraps **into** the plugin directory so that the existing `fraps.exe` gets overriden
3. Delete `fraps.dll`
4. Rename `fraps32.dll` to `fraps.dll`
5. Manually run `fraps.exe` or make sure to run LCDHype with administrator privileges to allow the plugin to start *Fraps* automatically
6. Run LCDHype
7. Create a script with the code line `%Plugin.LCDFraps.GetFPS()`